# This is a gitbook for Installing gitBook



## What you need


- git
- NodeJS
- gitbook-cli
- gitbook-editor

## Installation

 - [INSTALL.md](INSTALL)

## Tutorial

 - [TUTORIAL.md](TUTORIAL)

## Example :

  this book code is located at :

  <br><https://gitlab.com/holoMedia/holoBooks/gitbook-installation>
