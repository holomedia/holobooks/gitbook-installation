---
---
# Installing a local gitBook client


1. using gitbook Legacy to compile and render the markdown-books
 
pre-requisite:

 - Git : (fork-git or other prefered git client)
 - NodeJS : <https://nodejs.org/en/>
   <br>(see also holoKit/bin/get_npm.sh)

 linux : ``sudo apt-get install npm -y``


install:

```
npm install -g -save-dev gitbook-cli
gitbook ls

tutorial:

 - gitbook init
 
 

see also [1]: <https://legacy.gitbook.com/>

2. using a local gitbook editor

 - install see : https://www.one-tab.com/page/ghwlJZ16TwW4PQUdmF7wzw

see also [2]: <https://legacy.gitbook.com/editor>
